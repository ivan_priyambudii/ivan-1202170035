@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @foreach($post as $ps)
            <div class="card">

                <div class="card-header">
                    <div class="row">
                        <div class="col-md-1"> 
                            <img src="{{asset('img/foto.jpg')}}" style="border-radius: 50%; width: 30px; height: 30px;"></div>
                            <div class="col-md-11">
                                {{ \App\User::where(['id' => $ps->user_id])->first()->name}}
                            </div>
                        </div>                
                    </div>

                    <div class="card-body">
                     <center>
                        <img src="{{ str_replace('public/','', $ps->image) }}" style="position: relative; width: 100%;">
                    </center>
                </div>

                <div class="card-header">
                    <b>{{ \App\User::where(['id' => $ps->user_id])->first()->email }}</b>
                    <br>
                    {{$ps->caption}}
                </div>
            </div>
            <br>
            <br>
            @endforeach

        </div>
    </div>
</div>
@endsection
