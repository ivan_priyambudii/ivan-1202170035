<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Post;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\User;
use Carbon\Carbon;


$factory->define(Post::class, function (Faker $faker) {
	$data = User::all()->count();
	return [
		DB::table('posts')->insert([
		'user_id' => rand(1,$data),
		'caption' => Str::random(10),
		'image' => $faker->image('public/img',400,300),
        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
    ])];
});
