<!DOCTYPE html>
<html>
<head>
	<title>Menu</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans&display=swap" rel="stylesheet">
</head>
<body>

	<div class="konten-menu">
		<div class="grid-container outline">
			<div class="row">
				<div class="col-3" style="text-align: center;">
					<div style="position: fixed;">
						<h1 class="font-judul">~Data Driver Ojol~</h1>
						<ul>
							<li><b>Nama</b>
								<br>
								<p><?= $_GET["NamaDriver"]; ?></p>
							</li><br>
							<li><b>Nomor Telepon</b>
								<br>
								<p><?= $_GET["NomorTelepon"]; ?></p>
							</li><br>
							<li><b>Tanggal</b>
								<br>
								<p><?= $_GET["tanggal"]; ?></p>
							</li><br>
							<li><b>Asal Driver</b>
								<br>
								<p><?= $_GET["dari"]; ?></p>
							</li><br>
							<li><b>Bawa Kantong</b><br>
								<p>
									<?php

									if (!empty($_GET['kantong'])) {
										echo "Ya";
									} else {
										echo "Tidak";
									}

									?>
								</p>
								<br>
							</li>
							<br>
						</ul>
						<form action="form.html" method="get">
							<button class="button-kembali" type="submit"><< Kembali</button>
						</form>
					</div>
				</div>

				<div class="col-3">
					<div style="text-align: center;">
						<h1 class="font-judul">~Menu~</h1>
						<p class="font-judul">Pilih Menu.</p>
					</div>

					<center>
						<form action="nota.php" method="post">
							<table id="normal">
								<tr>
									<td><input type="checkbox" name="harga[]" id="harga" value="28000"><b>Es Coklat Susu</b></td>
									<td><div class="font-judul tabel-tengah">Rp. 28.000,-</div></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="harga[]" id="harga" value="18000"><b>Es Susu Matcha</b></td>
									<td><div class="font-judul tabel-tengah">Rp. 18.000,-</div></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="harga[]" id="harga" value="15000"><b>Es Susu Mojicha</b></td>
									<td><div class="font-judul tabel-tengah">Rp. 15.000,-</div></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="harga[]" id="harga" value="30000"><b>Es Matcha Latte</b></td>
									<td><div class="font-judul tabel-tengah">Rp. 30.000,-</div></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="harga[]" id="harga" value="21000"><b>Es Taro Susu</b></td>
									<td><div class="font-judul tabel-tengah">Rp. 21.000,-</div></td>
								</tr>
								<tr>
									<td><b>Nomor Order</b></td>
									<td><input class="ukuran-form form-apik" type="number" name="noorder"></td>
								</tr>
								<tr>
									<td><b>Nama Pemesan</b></td>
									<td><input class="ukuran-form form-apik" type="text" name="nama"></td>
								</tr>
								<tr>
									<td><b>Email</b></td>
									<td><input class="ukuran-form form-apik" type="email" name="email"></td>
								</tr>
								<tr>
									<td><b>Alamat Order</b></td>
									<td><input class="ukuran-form form-apik" type="text" name="alamat"></td>
								</tr>
								<tr>
									<td><b>Member</b></td>
									<td>
										<input type="radio" name="member" value="Ya">Ya
										<input type="radio" name="member" value="Tidak">Tidak
									</div>
								</td>
							</tr>
							<tr>
								<td><b>Metode Pembayaran</b></td>
								<td>
									<select class="ukuran-form form-apik" name="pembayaran">
										<option value="">
											Pilih Metode Pembayaran
										</option>
										<option value="Cash">Cash</option>
										<option value="E-Money(OVO/Gopay)">E-Money(OVO/Gopay)</option>
										<option value="Credit Card">Credit Card</option>
										<option value="Lainnya">Lainnya</option>
									</select>
								</td>
							</tr>
							<tr>
								<td colspan="2"><input class="button-cetak" type="submit" name="submit" id="submit" value="Cetak Nota"></td>
							</tr>
						</table>
					</form>
					<br>
					<br>
				</center>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#submit').click(function(){
		if($('input').val() != ''){
			confirm('Apakah anda yakin ?');
		}
	});
</script>
</body>
</html>