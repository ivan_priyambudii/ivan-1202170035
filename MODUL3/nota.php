<!DOCTYPE html>
<html>
<head>
	<title>Struk Belanja</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans&display=swap" rel="stylesheet">
</head>
<body>

	<div class="konten">
		<center><h1 class="font-judul">Transaksi</br>Pemesanan</h1></center>
		<center><p class="font-judul">Terimakasih telah berbelanja dengan Kopi Susu Duarrr!</p></center>
		<center>
			<form action="">
				<div class="grid-container outline">
					<?php

					if(!empty($_POST['harga'])){
						$total = array_sum($_POST['harga']);

						if ($_POST["member"] == 'Ya') {
							$diskon = 10/100 * $total;
							$hasil = $total - $diskon;
						}else{
							$hasil = $total;
						}
					}
					?>
					<table id="haha">
						<tr>
							<td colspan="2"><center><h1 class="font-judul">Rp. <?= number_format($hasil) ?>.00,-</h1></center></td>
						</tr>
						<tr>
							<td><div class="teks-form"><b>ID</b></div></td>
							<td><div class="font-judul"><p><?= $_POST["noorder"]; ?></p></div></td>
						</tr>
						<tr>
							<td><div class="teks-form"><b>Nama</b></div></td>
							<td><div class="font-judul"><p><?= $_POST["nama"]; ?></p></div></td>
						</tr>
						<tr>
							<td><div class="teks-form"><b>Email</b></div></td>
							<td><div class="font-judul"><p><?= $_POST["email"]; ?></p></div></td>
						</tr>
						<tr>
							<td><div class="teks-form"><b>Alamat</b></div></td>
							<td><div class="font-judul"><p><?= $_POST["alamat"]; ?></p></div></td>
						</tr>
						<tr>
							<td><div class="teks-form"><b>Member</b></div></td>
							<td><div class="font-judul"><p><?= $_POST["member"]; ?></p></div></td>
						</tr>
						<tr>
							<td><div class="teks-form"><b>Pembayaran</b></div></td>
							<td><div class="font-judul"><p><?= $_POST["pembayaran"]; ?></p></div></td>
						</tr>
					</table>
				</div>
			</form>
		</center>
	</div>

</body>
</html>