<!DOCTYPE html>
<?php session_start(); ?>
<html>
<head>
	<title>Profil</title>

	<link rel="stylesheet" type="text/css" href="asset/css/mat.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="asset/css/materialize.css">
	<link rel="stylesheet" type="text/css" href="asset/css/materialize.min.css">

	<style type="text/css">
		body {
			display: flex;
			min-height: 100vh;
			flex-direction: column;
		}

		main {
			flex: 1 0 auto;
		}
	</style>

</head>
<body>
	<header>
		<ul id="dropdown1" class="dropdown-content">
			<li><a class="prodase-color-teks" href="profil.php">Profil</a></li>
			<li class="divider"></li>
			<li><a class="prodase-color-teks" href="logout.php">Logout</a></li>
		</ul>
		<nav class="no-shadow white" style="border-bottom: 1px solid #bdbdbd;">
			<div class="nav-wrapper">
				<a href="#" class="brand-logo prodase-color-teks margin-left-70"><img src="asset/img/EAD.png" style="width: 10%; padding-top: 12px;"></a>
				<ul class="right hide-on-med-and-down margin-right-20">
					<?php 
					if(empty($_SESSION['email'])){  ?>
						<li><a class="waves-effect prodase-color-teks modal-trigger" href="#modal2">Login</a></li>
						<li><a class="waves-effect prodase-color-teks modal-trigger" href="#modal3">Register</a></li>
					<?php } else { ?>
						<li><a class="waves-effect prodase-color-teks" href="cart.php"><i class="material-icons right">shopping_cart</i></a>
						</li>
						<?php 
						include 'koneksi.php';
						$id=$_SESSION['id'];
						$data = mysqli_query($connect, "SELECT * FROM `users` WHERE `id`='$id'");
						while($d = mysqli_fetch_array($data)){
							?>
							<li><a class="waves-effect prodase-color-teks dropdown-trigger" href="#!" data-target="dropdown1"><?php echo $d['username']; ?><i class="material-icons right">arrow_drop_down</i></a></li>
						<?php } ?>
					<?php } ?>
				</ul>
			</div>
		</nav>
	</header>

	<main>
		<div class="container">
			
			<div class="card tabelhover col s10 pull-s1 m7 no-padding radius-10">

				<div class="white head-pop">
					<a href="index.php"><button class="white no-shadow btn  radius-left-top-10 cappital" style="color: #8f8f8f;"><i class="material-icons left">arrow_back</i>Back</button></a>
				</div>

				<div class="card-content row no-margin-bottom">
					<br>
					<div class="row valign-wrapper">
						<div class="col s4 valign center" style="margin-right: 20px;">
							<h3 class="prodase-color-teks">Profil</h3>
						</div>
						<div class="col s8" style="border-left:solid 1px #eeeeee  ; padding-left: 20px; padding-right: 40px;">
							<form action="edit.php" method="POST">
								<?php 
								include 'koneksi.php';
								$id=$_SESSION['id'];
								$data = mysqli_query($connect, "SELECT * FROM `users` WHERE `id`='$id'");
								while($d = mysqli_fetch_array($data)){
									?>
									<div class="input-field">
										<input name="email" id="email" type="email" class="validate" value="<?php echo $d['email']; ?>">
										<label for="email">Email</label>
									</div>

									<div class="input-field">
										<input name="username" id="username" type="text" class="validate" value="<?php echo $d['username']; ?>">
										<label for="username">Username</label>
									</div>

									<div class="input-field">
										<input name="mobile_number" id="mobile_number" type="number" class="validate" value="<?php echo $d['mobile_number']; ?>">
										<label for="mobile_number">Mobile Number</label>
									</div>

									<div class="input-field">
										<input name="password" id="password" type="password" class="validate" value="<?php echo $d['password']; ?>">
										<label for="username">Password</label>
									</div>

									<div class="input-field">
										<input name="password" id="password" type="password" class="validate" value="<?php echo $d['password']; ?>">
										<label for="password">Confirm Password</label>
									</div>

									<input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>">

									<button class="waves-effect waves-light btn prodase-color" name="submit" value="submit" type="submit" style="width: 100%;">Save</button>
									<br><br>
									<a href="index.php" class="waves-effect waves-light btn red lighten-3" style="width: 100%;">Cancel</a>
								<?php } ?>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<footer class="prodase-color">
		<div class="footer-copyright">
			<div class="container center white-text" style="padding: 0.2px 0 0.2px 0;">
				<p>© 2019 Copyright Ivan Priyambudi | 1202170035 | SI4105</p>

			</div>
		</div>
	</footer>

	<script src="https://kit.fontawesome.com/a076d05399.js"></script>
	<script src='https://storage.googleapis.com/code.getmdl.io/1.1.0/material.min.js'></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="asset/js/materialize.min.js"></script>
	<script type="text/javascript">
		$(".dropdown-trigger").dropdown();
	</script>
</body>
</html>