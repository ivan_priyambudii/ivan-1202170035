<!DOCTYPE html>
<?php session_start(); ?>
<html>
<head>
	<title>Cart</title>

	<link rel="stylesheet" type="text/css" href="asset/css/mat.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="asset/css/materialize.css">
	<link rel="stylesheet" type="text/css" href="asset/css/materialize.min.css">

	<style type="text/css">
		body {
			display: flex;
			min-height: 100vh;
			flex-direction: column;
		}

		main {
			flex: 1 0 auto;
		}
	</style>
	
</head>
<body>
	<header>
		<ul id="dropdown1" class="dropdown-content">
			<li><a class="prodase-color-teks" href="profil.php">Profil</a></li>
			<li class="divider"></li>
			<li><a class="prodase-color-teks" href="logout.php">Logout</a></li>
		</ul>
		<nav class="no-shadow white" style="border-bottom: 1px solid #bdbdbd;">
			<div class="nav-wrapper">
				<a href="#" class="brand-logo prodase-color-teks margin-left-70"><img src="asset/img/EAD.png" style="width: 10%; padding-top: 12px;"></a>
				<ul class="right hide-on-med-and-down margin-right-20">
					<?php 
					if(empty($_SESSION['email'])){  ?>
						<li><a class="waves-effect prodase-color-teks modal-trigger" href="#modal2">Login</a></li>
						<li><a class="waves-effect prodase-color-teks modal-trigger" href="#modal3">Register</a></li>
					<?php } else { ?>
						<li><a class="waves-effect prodase-color-teks" href="cart.php"><i class="material-icons right">shopping_cart</i></a>
						</li>
						<?php 
						include 'koneksi.php';
						$id=$_SESSION['id'];
						$data = mysqli_query($connect, "SELECT * FROM `users` WHERE `id`='$id'");
						while($d = mysqli_fetch_array($data)){
							?>
							<li><a class="waves-effect prodase-color-teks dropdown-trigger" href="#!" data-target="dropdown1"><?php echo $d['username']; ?><i class="material-icons right">arrow_drop_down</i></a></li>
						<?php } ?>
					<?php } ?>
				</ul>
			</div>
		</nav>
	</header>

	<main>
		<div class="container">
			<div class="card tabelhover col s10 pull-s1 m7 no-padding radius-10">

				<div class="white head-pop">
					<a href="index.php"><button class="white no-shadow btn  radius-left-top-10 cappital" style="color: #8f8f8f;"><i class="material-icons left">arrow_back</i>Back</button></a>
				</div>

				<div class="card-content row no-margin-bottom">
					<form>
						<center>
							<table>
								<thead>
									<tr>
										<th>No</th>
										<th>Product</th>
										<th>Price</th>
										<th style="width: 15%;"></th>
									</tr>
								</thead>

								<?php 
								include 'koneksi.php';
								$id=$_SESSION['id'];
								$data = mysqli_query($connect, "SELECT * FROM `cart_table` WHERE `user_id`='$id'");
								$no=0;
								foreach ($data as $row) : 
									$no++;?>

									<tbody>
										<tr>
											<td><?= $no; ?></td>
											<td><?= $row['product']; ?></td>
											<td><?= $row['price']; ?></td>
											<td><a class="btn-floating waves-effect waves-light red" href="delete.php?id=<?= $row['id']; ?>"><i class="material-icons">close</i></a></td>
										</tr>
									<?php endforeach; ?>
									<tr class="no-border">
										<td class="center" colspan="2">Jumlah</td>
										<td>
											<?php
											$id=$_SESSION['id'];
											$data = mysqli_query($connect, "SELECT * FROM `cart_table` WHERE `user_id`='$id'");
											$no=0;
											$jumlah=0;
											foreach ($data as $row) : 

												$jumlah += $row['price'];
											endforeach;
											echo $jumlah;
											?>
										</td>
									</tr>
								</tbody>
							</table>
						</center>
					</form>
				</div>
			</div>
		</div>
	</main>

	<footer class="prodase-color">
		<div class="footer-copyright">
			<div class="container center white-text" style="padding: 0.2px 0 0.2px 0;">
				<p>© 2019 Copyright Ivan Priyambudi | 1202170035 | SI4105</p>

			</div>
		</div>
	</footer>

	<script src="https://kit.fontawesome.com/a076d05399.js"></script>
	<script src='https://storage.googleapis.com/code.getmdl.io/1.1.0/material.min.js'></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="asset/js/materialize.min.js"></script>
	<script type="text/javascript">
		$(".dropdown-trigger").dropdown();
	</script>
</body>
</html>