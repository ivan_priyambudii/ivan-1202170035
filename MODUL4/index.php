<!DOCTYPE html>
<?php session_start(); ?>
<html>
<head>
	<title>Home</title>

	<link rel="stylesheet" type="text/css" href="asset/css/mat.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="asset/css/materialize.css">
	<link rel="stylesheet" type="text/css" href="asset/css/materialize.min.css">

</head>
<body>
	<header>
		<ul id="dropdown1" class="dropdown-content">
			<li><a class="prodase-color-teks" href="profil.php">Profil</a></li>
			<li class="divider"></li>
			<li><a class="prodase-color-teks" href="logout.php">Logout</a></li>
		</ul>
		<nav class="no-shadow white" style="border-bottom: 1px solid #bdbdbd;">
			<div class="nav-wrapper">
				<a href="#" class="brand-logo prodase-color-teks margin-left-70"><img src="asset/img/EAD.png" style="width: 10%; padding-top: 12px;"></a>
				<ul class="right hide-on-med-and-down margin-right-20">
					<?php 
					if(empty($_SESSION['email'])){  ?>
						<li><a class="waves-effect prodase-color-teks modal-trigger" href="#modal2">Login</a></li>
						<li><a class="waves-effect prodase-color-teks modal-trigger" href="#modal3">Register</a></li>
					<?php } else { ?>
						<li><a class="waves-effect prodase-color-teks" href="cart.php"><i class="material-icons right">shopping_cart</i></a>
						</li>
						<?php 
						include 'koneksi.php';
						$id=$_SESSION['id'];
						$data = mysqli_query($connect, "SELECT * FROM `users` WHERE `id`='$id'");
						while($d = mysqli_fetch_array($data)){
							?>
							<li><a class="waves-effect prodase-color-teks dropdown-trigger" href="#!" data-target="dropdown1"><?php echo $d['username']; ?><i class="material-icons right">arrow_drop_down</i></a></li>
						<?php } ?>
					<?php } ?>
				</ul>
			</div>
		</nav>
	</header>

	<main>
		<div class="container" style="padding-top: 50px;" >
			<div class="row">
				<div class="col s12">
					<div class="card-panel prodase-color">
						<span class="white-text"><h3>Hello Coders</h3></span>
						<span class="white-text">
							Welcome to our store, please take a look for the product you might buy
						</span>
					</div>
				</div>
				<div class="col s4">
					<div class="card">
						<div class="card-panel2 no-shadow" style="padding: 50px 0 50px 0;">
							<div style="text-align: center;">
								<i class="large material-icons fas fa-globe color-icon"></i>
							</div>
						</div>
						<div class="card-content">
							<h6 class="center"><b>Learning Basic Web Programming</b></h6>
							<h6 class="center">Rp 210.000,00</h6><br>
							<p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
						</div>
						<div class="card-action">
							<?php 
							if(empty($_SESSION['email'])){  ?>
								<button class="waves-effect waves-light btn card-has" type="button" style="width: 100%;">Buy</button>
							<?php } else { ?>
								<form action="tambahcart.php" method="get">
									<input type="hidden" name="item" value="210000">
									<input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>">
									<button class="waves-effect waves-light btn card-has" type="submit" style="width: 100%;">Buy</button>
								</form>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="col s4">
					<div class="card">
						<div class="card-panel2 no-shadow" style="padding: 50px 0 50px 0;">
							<div style="text-align: center;">
								<i class="large material-icons fab fa-java color-icon"></i>
							</div>
						</div>
						<div class="card-content">
							<h6 class="center"><b>Starting Programming in Java</b></h6>
							<h6 class="center">Rp 150.000,00</h6><br>
							<p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
						</div>
						<div class="card-action">
							<?php 
							if(empty($_SESSION['email'])){  ?>
								<button class="waves-effect waves-light btn card-has" type="button" style="width: 100%;">Buy</button>
							<?php } else { ?>
								<form action="tambahcart.php" method="get">
									<input type="hidden" name="item" value="150000">
									<input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>">
									<button class="waves-effect waves-light btn card-has" type="submit" style="width: 100%;">Buy</button>
								</form>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="col s4">
					<div class="card">
						<div class="card-panel2 no-shadow" style="padding: 50px 0 50px 0;">
							<div style="text-align: center;">
								<i class="large material-icons fab fa-python color-icon"></i>
							</div>
						</div>
						<div class="card-content">
							<h6 class="center"><b>Starting Programming in Python</b></h6>
							<h6 class="center">Rp 200.000,00</h6><br>
							<p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>

						</div>
						<div class="card-action">
							<?php 
							if(empty($_SESSION['email'])){  ?>
								<button class="waves-effect waves-light btn card-has" type="button" style="width: 100%;">Buy</button>
							<?php } else { ?>
								<form action="tambahcart.php" method="get">
									<input type="hidden" name="item" value="200000">
									<input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>">
									<button class="waves-effect waves-light btn card-has" type="submit" style="width: 100%;">Buy</button>
								</form>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<footer class="prodase-color">
		<div class="footer-copyright">
			<div class="container center white-text" style="padding: 0.2px 0 0.2px 0;">
				<p>© 2014 Copyright Ivan Priyambudi | 1202170035 | SI4105</p>

			</div>
		</div>
	</footer>

	<!-- modal login-->
	<div id="modal2" class="modal no-shadow">
		<div class="prodase-color">
			<button href="#!" class="modal-close prodase-color no-shadow back btn cappital"><i class="material-icons left">arrow_back</i>Back</button>
		</div>
		<div class="modal-content">
			<div class="row valign-wrapper">
				<div class="col s4 valign center" style="margin-right: 20px;">
					<h3 class="prodase-color-teks">Login</h3>
				</div>
				<div class="col s8" style="border-left:solid 1px #eeeeee  ; padding-left: 20px;">
					<form action="login.php" method="POST">
						<div class="input-field">
							<input id="email" name="email" value="" type="email" class="validate">
							<label for="email">Email</label>
						</div>

						<div class="input-field">
							<input id="password" name="password" value="" type="password" class="validate">
							<label for="username">Password</label>
						</div>

						<button class="waves-effect waves-light btn prodase-color" name="submit" value="submit" type="submit" style="width: 100%;">Login</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- modal login-->

	<!-- modal register-->
	<div id="modal3" class="modal no-shadow">
		<div class="prodase-color">
			<button href="#!" class="modal-close prodase-color no-shadow back btn cappital"><i class="material-icons left">arrow_back</i>Back</button>
		</div>
		<div class="modal-content">
			<div class="row valign-wrapper">
				<div class="col s4 valign center" style="margin-right: 20px;">
					<h3 class="prodase-color-teks">Register</h3>
				</div>
				<div class="col s8" style="border-left:solid 1px #eeeeee  ; padding-left: 20px;">
					<form action="register.php" method="POST">
						<div class="input-field">
							<input name="email" id="email" type="email">
							<label for="email">Email</label>
						</div>

						<div class="input-field">
							<input name="username" id="username" type="text">
							<label for="username">Username</label>
						</div>

						<div class="input-field">
							<input type="password" class="form-control" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,12}$" title="8-12 that contains uppercase, lowercase, and numeric without whitespace" class="form-control" name="password" id="password" value="">
							<label for="inputPassword">Password</label>
						</div>

						<div class="input-field">
							<input type="password" class="form-control" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,12}$" title="8-12 that contains uppercase, lowercase, and numeric without whitespace" class="form-control" name="confirmPass" id="confirmpass">
							
							<label for="inputPassword">Password (Confirm)</label>
						</div>

						<button class="waves-effect waves-light btn prodase-color" name="submit" value="submit" type="submit" style="width: 100%;">Register</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- modal register-->

	<script src="https://kit.fontawesome.com/a076d05399.js"></script>
	<script src='https://storage.googleapis.com/code.getmdl.io/1.1.0/material.min.js'></script>
	
	<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
	
	<script src="asset/js/materialize.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.modal').modal();
		});

		$(".dropdown-trigger").dropdown();
	</script> 
	<script>
		function Validate() {
			var password = document.getElementById("password").value;
			var confirmPassword = document.getElementById("confirmpass").value;
			if (password != confirmPassword) {
				$(document).ready(function(){
  // $('#modalAlert').modal('show');
  $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
  	$("#success-alert").slideUp(500);
  });
});
				return false;
			}
			return true;
		}
		$("#password").keyup(function(){
		});
		$("#confirmpass").keyup(function(){
			if ($(this).val() != $('#password').val()) {
				$('#alert2').empty().html(' ');
				$('#alert2').append('<p style="color:red;">Password tidak sama</p>');
			} else {
				$('#alert2').empty().html(' ');
			}
		});
	</script>
</body>
</html>