<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class coment extends Model
{
	protected $table = 'komentar_posts';

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
