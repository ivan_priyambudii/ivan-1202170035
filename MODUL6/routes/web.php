<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/detail/{id}', 'HomeController@detail')->name('detail');
Route::get('/home/profile', 'HomeController@profile')->name('profile');
Route::get('/home/newpost', 'HomeController@newpost')->name('newpost');
Route::get('/home/editprofile/{id}', 'HomeController@editprofile')->name('editprofile');

Route::post('/home/addkomen', 'HomeController@addkomen')->name('addkomen');
Route::post('/home/addkomen2', 'HomeController@addkomen2')->name('addkomen2');

Route::post('/home/editprofile/editprofil/{id}', 'HomeController@editprofil')->name('editprofil');

Route::post('/home/newpost/newpostaction', 'HomeController@newpostaction')->name('newpostaction');
Route::post('/home/likes', 'HomeController@likes')->name('likes');

